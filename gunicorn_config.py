import os

workers_var = int(os.environ.get('workers', 2))
print("workers_var:", workers_var)

# Get the number of workers from the environment variable
workers = workers_var

worker_class = "uvicorn.workers.UvicornWorker"

bind = "0.0.0.0:5001"  # Bind to all available network interfaces on port 5001

reload = True
