import logging
from dataclasses import dataclass
from time import time

from fastapi import FastAPI, Form, UploadFile

from . import vad
from .vad import InferSpeechProbResult
from . import utils

app = FastAPI()

logger = utils.create_logger("api")


@dataclass
class BasicResponse:
    success: bool
    message: str = None


@dataclass
class SpeechProbResponse:
    success: bool
    result: InferSpeechProbResult


@app.get("/")
def index():
    data = BasicResponse(success=True)

    return data


@app.post("/speech_probability")
async def speech_probability(audio_file: UploadFile = Form(...), tag: str = Form(...)):
    logger_name = f"speech_probability:{tag}"
    child_logger = logger.getChild(logger_name)

    child_logger.info(f"Speech probability request")

    try:
        audio_data = await audio_file.read()

        audio_file_temp = utils.load_audio_tempfile(audio_data)
        
        with audio_file_temp:
            speech_prob_result = vad.infer_speech_prob(audio_file_temp.name, tag)

        child_logger.info(f"Request successful")

        data = SpeechProbResponse(success=True, result=speech_prob_result)

        return data

    except Exception as e:
        logger.exception("An error occurred:")

        error_message = str(e)

        data = BasicResponse(success=False, message=error_message)

        return data


@app.exception_handler(Exception)
async def generic_exception_handler(request, exc):
    error_message = f"An error occurred: {str(exc)}"

    data = BasicResponse(success=False, message=error_message)

    return data
